<?php

/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/


/**
 * SERVICE CONFIGURATION
 */


/**
 * Set this path to your backend (e.g. dotproject) correctly!
 *
 * Default: '../..'
 *
 * The Default value assumes that the file hierarchy is like the following
 * /a/b/c/dotproject/service-0.70/service/service.php
 *
 */
define('DPDesktopBackPath','../..');


/**
 * select host-system for your DPDesktop (The name of the project management
 * software you are working with).
 *
 * Possible values are "dotproject" or "web2project".
 */
define('DPDesktopHost','dotproject');


/**
 * REMOTE CLIENT CONFIGURATION
 */

 /**
  * Configurate which time step size you prefer. E.g. if you choose "30" the
  * time for submit could be like "00:00", "00:30" or "01:00". In constrast if
  * you choose "1" you can specifiy your working time in the most accurate way.
  * This means, that all values between "00:00" and "00:59" are possible.
  *
  * Possible values are "1","2","5","10","15","20","30".
  * 
  * Any other time step size didn't make sense to us. If the value differs from
  * these mentioned above, the default value of the client will be used,
  * which is "10".
  */

define('CONFIG_TIME_STEP_SIZE', '30');

/**
 * Configurate whether the billable time feature should be available in the
 * client or not. Be careful! You have to recognize whether this feature is
 * supported by your host stystem. If it is not supported, it won't be enabled.
 */

define('CONFIG_ENABLE_BILLABLE_TIME_FEATURE_IF_SUPPORTED', true);


?>