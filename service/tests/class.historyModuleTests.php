<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of classconfigModuleTests
 *
 * @author User
 */
require_once 'PHPUnit/Framework.php';
require_once '../classes/class.dao.php';
require_once '../classes/class.dao_stub.php';
require_once '../classes/class.response_stub.php';
require_once '../classes/class.module.php';
require_once '../classes/class.module_history.php';

class historyModuleTests extends PHPUnit_Framework_TestCase {

    public function test_generatesValidXML() {
        $m = new module_history();
        $dao = new dao_stub();
        $m->setDao($dao);
        $resp = response_stub::getInstance();
        $m->setResponseObj($resp);
        $m->store(1, $this->getInputFixture());
        try {
            $d = new DOMDocument();
            $d->loadXML($resp->lastMessage);
        } catch(Exception $e) {
            $this->fail("XML not valid");
        }
        $m->load(1);
        $this->assertEquals(2,$resp->invokations);
        $this->assertEquals(false,$resp->containedError,"process contained errors:".$resp->lastInvokation."(".$resp->lastMessage.")");
        try {
            $d = new DOMDocument();
            $d->loadXML($resp->lastMessage);
        } catch(Exception $e) {
            $this->fail("XML not valid");
        }
    }

    public function test_storeAndLoadCircleWorks() {
        $m = new module_history();
        $m->setDao(new dao_stub());
        $resp = response_stub::getInstance();
        $m->setResponseObj($resp);
        $m->store(1, $this->getInputFixture());
        $this->assertEquals(1,$resp->invokations,"too much output generated");
        $this->assertEquals(false,$resp->containedError,"process contained errors:".$resp->lastInvokation."(".$resp->lastMessage.")");
        $m->load(1);
        $this->assertEquals(2,$resp->invokations,"too much output generated");
        $this->assertEquals(false,$resp->containedError,"process contained errors:".$resp->lastInvokation."(".$resp->lastMessage.")");
        $this->assertEquals($this->getDemoOutput(),$resp->lastMessage);
    }

    public function test_durationValuesAreKeptDuringStoreAndLoadTransformation() {
        for($h=0;$h<10;$h+=3) {
            for($i=0;$i<60;$i++) {
                $m = new module_history();
                $m->setDao(new dao_stub());
                $resp = response_stub::getInstance();
                $m->setResponseObj($resp);
                $duration = sprintf("%02d:%02d",$h,$i);
                $m->store(1, $this->getInputFixture($duration));
                $this->assertEquals(false,$resp->containedError,"process contained errors:".$resp->lastInvokation."(".$resp->lastMessage.")");
                $m->load(1);
                $this->assertEquals(false,$resp->containedError,"process contained errors:".$resp->lastInvokation."(".$resp->lastMessage.")");
                $this->assertEquals($this->getDemoOutput($duration),$resp->lastMessage," Due to store/load transformation the proper time ($duration) is lost.");
                unset($m);
            }
        }
    }

    protected function getInputFixture($duration="00:20") {
        $dom = new DOMDocument();
        $dom->loadXML('
        <root>
            <data>
                <log containerID="ID-3-1" summary="bla" comment="blubb" workedTime="'.$duration.'" ></log>
            </data>
        </root>
        ');
        $xpath = new DOMXPath($dom);
        $entries = $xpath->query("//root/data");
        return $entries->item(0);
    }

    protected function getDemoOutput($duration="00:20") {
        $str = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<root><response success=\"true\"><message>Execution successful</message></response><log id=\"1\" summary=\"bla\" comment=\"blubb\" workedTime=\"".$duration."\" date=\"2000-01-01 00:00:01\"><container id=\"ID-3-1\" owner=\"admin\" name=\"task\" priority=\"\" complete=\"0\" isTrackable=\"true\"/></log></root>\n";
        return $str;
    }
}
?>
