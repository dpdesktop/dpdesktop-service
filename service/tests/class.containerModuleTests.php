<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of classconfigModuleTests
 *
 * @author User
 */
require_once 'PHPUnit/Framework.php';
require_once '../classes/class.dao.php';
require_once '../classes/class.dao_stub.php';
require_once '../classes/class.response_stub.php';
require_once '../classes/class.module.php';
require_once '../classes/class.module_container.php';

class containerModuleTests extends PHPUnit_Framework_TestCase {
    public function test_generatesValidXML() {
        $m = new module_container();
        $m->setDao(new dao_stub());
        $resp = response_stub::getInstance();
        $m->setResponseObj($resp);
        $m->load($userID);
        $this->assertEquals(1,$resp->invokations);

        try {
            $d = new DOMDocument();
            $d->loadXML($resp->lastMessage);
        } catch(Exception $e) {
            $this->fail("XML not valid");
        }

    }

    public function test_containerOutput() {

       /*
        $q->addQuery('c.company_id, c.company_name, c.company_phone1, c.company_email');
        $q->addTable('companies', 'c');
        */
        $companies = array(
            0 => array(
            "company_id" => 1,
            "company_name" => "company 1",
            "company_phone1" => "",
            "company_email" => "",
            ),

            1 => array(
            "company_id" => 2,
            "company_name" => "company 2",
            "company_phone1" => "",
            "company_email" => "",
            )

        );
        //$q->addQuery('p.project_id, p.project_name, p.project_color_identifier, p.project_company, c.company_name ');

        $projectsAndCompanies = array (
            0 => array(
            "project_id" => 1,
            "project_name" => "project 1",
            "project_color_identifier" => "#000000",
            "project_company" => "1",
            "company_name" => "company 1"
            )
        );
        //$q->addQuery('t.task_id, t.task_name, t.task_project, t.task_percent_complete, t.task_priority , u.user_username');
        $userTaskList = array(
            0 => array(
            "task_id" => 1,
            "task_name" => "task 1",
            "task_project" => 1,
            "task_percent_complete" => "80",
            "task_priority" => "",
            "user_username" => "user"
            )
        );


        $m = new module_container();


        $daoMock = $this->getMock('dao_stub');


        $resp = response_stub::getInstance();

        $m->setResponseObj($resp);
        $m->setDao( $daoMock );


        $daoMock->expects($this->any())
            ->method('getCompanies')
            ->will($this->returnValue($companies));

        $daoMock->expects($this->any())
            ->method('getProjectsAndCompanies')
            ->will($this->returnValue($projectsAndCompanies));

        $daoMock->expects($this->any())
            ->method('getUserTaskList')
            ->will($this->returnValue($userTaskList));


        $m->load(1);

        $d = new DOMDocument();
        $d->loadXML($resp->lastMessage);



        $this->assertEquals($d->getElementsByTagName("container")->length,3);


        //TODO create better test

    }
}
?>
