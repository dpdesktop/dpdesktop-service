<?php
require_once 'class.configModuleTests.php';
require_once 'class.containerModuleTests.php';
require_once 'class.historyModuleTests.php';

class testAllModules {
    public static function suite() {
        $suite = new PHPUnit_Framework_TestSuite('PHPUnit');
        $suite->addTestSuite(new PHPUnit_Framework_TestSuite('configModuleTests'));
        $suite->addTestSuite(new PHPUnit_Framework_TestSuite('containerModuleTests'));
        $suite->addTestSuite(new PHPUnit_Framework_TestSuite('historyModuleTests'));
        return $suite;
    }
}

?>