<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of classconfigModuleTests
 *
 * @author User
 */
require_once 'PHPUnit/Framework.php';
require_once '../classes/class.dao.php';
require_once '../classes/class.dao_stub.php';
require_once '../classes/class.response_stub.php';
require_once '../classes/class.module.php';
require_once '../classes/class.module_config.php';

class configModuleTests extends PHPUnit_Framework_TestCase {
    public function test_generatesValidXML() {        
        $m = new module_config();
        $m->setDao(new dao_stub());
        $resp = response_stub::getInstance();
        $m->setResponseObj($resp);
        $m->load($userID);
        $this->assertEquals(1,$resp->invokations);

        try {
            $d = new DOMDocument();
            $d->loadXML($resp->lastMessage);
        } catch(Exception $e) {
            $this->fail("XML not valid");
        }
    }
}
?>
