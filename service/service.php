<?php
/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/





// DO NOT CHANGE ANYTHING FROM HERE 
error_reporting(E_ALL);

include_once('classes/class.response_http.php');

// INCLUDE configuration file
include_once('config.php');

include_once('inc/strptime.inc.php');


include_once('classes/class.dao.php');
include_once('classes/class.module.php');
include_once('classes/class.module_factory.php');

// MAIN PART
$dom = new DOMDocument();
$xml = file_get_contents("php://input");

if(!$xml) response_http::getInstance()->requestFail();

$dom->loadXML( $xml );

$xpath = new DOMXPath($dom);
$entries = $xpath->query("//root/request");
$requestNode = $entries->item(0);

if(!(isset($daoclass) && class_exists($daoclass))) response_http::getInstance()->requestFail();
$dao = new $daoclass();

$userID = $dao->getUserId(
    $requestNode->getAttribute("username"),
    $requestNode->getAttribute("password")
);
if(!$userID) response_http::getInstance()->authFail();

$m = module_factory::createModule($requestNode->getAttribute("module"),$dao,'response_http');
if(!$m) response_http::getInstance()->requestFail();

switch($requestNode->getAttribute("method")) {
    case "LOAD"	:
        $m->load( $userID );
        break;

    case "STORE":
        $entries = $xpath->query("//root/data");
        $dataElement = $entries->item(0);
        $m->store( $userID, $dataElement );
        break;

    default:
        response_http::getInstance()->requestFail();
}

?>