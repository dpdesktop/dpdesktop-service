<?php
/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

/**
 * Description of class module_config
 *
 * @author Tolleiv Nietsch
 */
class module_config extends module {

    private function convertBooleanToString($value) {
        if($value) {
            return "true";
        }

        return "false";
    }


    public function load($userID) {

        $timeStepSize = $this->dom->createElement("timeStepSize");
        $timeStepSize->setAttribute("value", CONFIG_TIME_STEP_SIZE);

        $enableBillableTime = $this->dom->createElement("enableBillableTime");
        $enableBillableTime->setAttribute("value", $this->convertBooleanToString($this->dao->hasBillableHoursFeature()) );

        $this->domRoot->appendChild( $timeStepSize );
        $this->domRoot->appendChild( $enableBillableTime );

        $this->printOk();

    }
    public function store($userID, DOMElement $dataElement) {
        //
    }
}

?>
