<?php
/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

/**
 * Description of class module_container
 *
 * @author Tolleiv Nietsch
 */
class module_container extends module {

    public function load($userID) {

        $priority = $this->dao->getSysVal("TaskPriority");
        $comp = $this->dao->getCompanies();
        $proj = $this->dao->getProjectsAndCompanies();
        $task = $this->dao->getUserTaskList($userID);

        foreach($proj as $_) {
            $my_proj[$_['project_company']][] = $_;
        }
        foreach($task as $_) {
            $my_task[$_['task_project']][] = $_;
        }

        foreach($comp as $_) {

            // Company attributes
            $currentCompanyHasAnyProject = false;
            $company = $this->dom->createElement('container');
            $company->setAttribute("name", $_['company_name']);
            $company->setAttribute("color","abd4d5");
            $company->setAttribute("id", "ID-1-".$_['company_id']);
            $company->setAttribute("phone1", $_['company_phone1']);
            $company->setAttribute("email", $this->out($_['company_email']) );
            $company->setAttribute("isTrackable", "false" );

            if(!isset($my_proj[$_['company_id']]))
            continue;

            foreach($my_proj[$_['company_id']] as $__) {

                // Project attributes
                $project = $this->dom->createElement('container');
                $project->setAttribute("id", "ID-2-".$__['project_id']);
                $project->setAttribute("name", $this->out($__['project_name']) );
                $project->setAttribute("color", $__['project_color_identifier']);
                $project->setAttribute("isTrackable", "false" );

                if(!isset($my_task[$__['project_id']]) || count($my_task[$__['project_id']])==0)  // do not show project if no tasks available for this project
                continue;

                foreach($my_task[$__['project_id']] as $___) {

                    // Task attributes
                    $task = $this->dom->createElement('container');
                    $task->setAttribute("id", "ID-3-" . $___['task_id']);
                    $task->setAttribute("owner", $this->out($___['user_username']) );
                    $task->setAttribute("name", $this->out($___['task_name']) );
                    $task->setAttribute("priority", $priority[$___['task_priority']] );
                    $task->setAttribute("complete", $___['task_percent_complete'] );


                    /**
                     * Here you can set Service Level Agreement Parameters for
                     * specific objects, e.g a task. This Data will be shown on
                     * information popup in the objective-selection-area of
                     * dpDesktop.
                     * @see class.dao_project.php#getUserTaskList(..)
                     */

                    //$task->setAttribute("sla-tracking-max", $___['sla_tracking_max']");
                    //$task->setAttribute("sla-tracking-current", $___['sla_tracking_current']");

                    /**
                     * Maybe you want to apply a new color to such an task.
                     */

                    //$task->setAttribute("color", "00FF00");

                    $task->setAttribute("isTrackable", "true" );

                    $project->appendChild( $task );
                }

                $company->appendChild( $project );
                $currentCompanyHasAnyProject = true;
            }

            // DO NOT SHOW COMPANY IF THERE IS NO TASK AVAILABLE IN ANY PROJECT OR THERE IS NO PROJECT
            if($currentCompanyHasAnyProject)
                $this->domRoot->appendChild( $company );
        }
        $this->printOk();
    }



    public function store($userID, DOMElement $dataElement) {
        //
    }

} 
