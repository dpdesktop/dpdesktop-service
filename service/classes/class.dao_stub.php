<?php
/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

/**
 * Description of class dao_stub
 *
 * @author Tolleiv Nietsch
 */
class dao_stub extends dao {

    public function getCompanies() {        
        return array();
    }

    public function getProjectsAndCompanies() {
        return array();
    }

    public function getTaskById($id) {
        return $id;
    }

    public function getUserTaskList($userID) {
        return array();
    }

   protected $log;
   public function getUserHistory($userID) {
        return $this->log[$userID];
   }

   public function addTaskLog($id,$desc,$name,$userID,$worked,$billable) {
       $in = array(
            "task_log_id"=>1,
            "task_log_task"=>$id,
            "task_log_description"=>$desc,
            "task_log_name"=>$name,
            "task_log_creator"=>$userID,
            "task_log_hours"=>$worked,            
            "task_log_date"=> '2000-01-01 00:00:01',
            "task_id"=>1,
            "task_name"=>"task",
            "task_project"=>1,
            "task_percent_complete"=>0,
            "task_priority"=>1,
            "company_name"=>"company",
            "project_name"=>"project",
            "user_username"=>"admin"
        );
        if($this->hasBillableHoursFeature()) {
            $inp["my_task_log_billable_hours"] = $billable;
        }
        $this->log[$userID][] = $in;
   }

   public function updateTaskProgress($id,$progress) {
        return;
   }
   public function getUserId($user,$pass) {
        return '1';
   }

   public function getSysVal($key) {
        $reg=array(
            "TaskPriority"=>"1"
        );
        return $reg[$key];
   }
}
?>