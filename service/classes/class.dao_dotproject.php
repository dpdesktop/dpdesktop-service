<?php
/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

if(!defined('DPDesktopBackPath')) {
    define('DPDesktopBackPath','..');
}

include(DPDesktopBackPath.'/base.php');

ob_start();     // this is done because this include produces an echo "", which leads to parse errors
include_once(DP_BASE_DIR.'/includes/config.php');
ob_clean();
include(DP_BASE_DIR.'/includes/main_functions.php');
include(DP_BASE_DIR.'/includes/db_connect.php');

include(DP_BASE_DIR.'/classes/query.class.php');
include(DP_BASE_DIR.'/classes/ui.class.php');

$AppUI = new CAppUI();		// needed for date class
include(DP_BASE_DIR.'/classes/date.class.php');

/**
 * Description of class dao_dotproject
 *
 * @author Tolleiv Nietsch
 */
class dao_dotproject extends dao{

    public function __construct() {
        $this->_checkBillableFeature();
    }

    private function _checkBillableFeature() {
        $list = db_loadList('SHOW COLUMNS FROM task_log');
        $fields=array();
        if(is_array($list)) {
            foreach($list as $field) {
                $fields[]=$field["Field"];
            }
        }
        $this->setBillableHoursFeature(in_array('my_task_log_billable_hours',$fields));
        
    }

    public function getCompanies() {
        $q = new DBQuery();
        $q->addQuery('c.company_id, c.company_name, c.company_phone1, c.company_email');
        $q->addTable('companies', 'c');
        $sql = $q->prepare();
        return db_loadList($sql);
    }

    public function getProjectsAndCompanies() {
        $q = new DBQuery();
        $q->addQuery('p.project_id, p.project_name, p.project_color_identifier, p.project_company, c.company_name ');
        $q->addOrder('p.project_name ASC');
        $q->addJoin('companies', 'c', ' c.company_id = p.project_company');
        $q->addTable('projects', 'p');
        $sql = $q->prepare();
        return db_loadList($sql);
    }

    public function getTaskById($id) {
        $q = new DBQuery();
        $q->addQuery('task_id');
        $q->addWhere('task_id = "'.intVal($id).'" ');
        $q->addTable('tasks');
        $sql = $q->prepare();
        return db_loadList($sql);
    }
    
    public function getUserTaskList($userID) {
        $q = new DBQuery();
        $q->addQuery('t.task_id, t.task_name, t.task_project, t.task_percent_complete, t.task_priority , u.user_username');
        $q->addOrder('t.task_name ASC'); 
        $q->addWhere('t.task_percent_complete < 100');
        $q->addWhere('ut.task_id = t.task_id ');
        $q->addWhere('ut.user_id = "'.intVal($userID).'" ');
        $q->addJoin('users', 'u', 'u.user_id = t.task_owner');		// Fetching Name for Owner, But maybe OwnerID does not exist anymore - so better to JOIN 
        $q->addTable('tasks', 't');
        $q->addTable('user_tasks', 'ut');

        $sql = $q->prepare();
        return db_loadList($sql);
    }


   public function getUserHistory($userID) {
        $q = new DBQuery();
        $q->addQuery('t.task_log_id, t.task_log_task, t.task_log_name, t.task_log_description, t.task_log_creator, t.task_log_hours, t.task_log_date, ts.task_name, c.company_name, p.project_name ');
        if($this->hasBillableHoursFeature()) {
            $q->addQuery('t.my_task_log_billable_hours');
        }
        $q->addQuery('ts.task_id, ts.task_name, ts.task_project, ts.task_percent_complete, ts.task_priority , ut.user_username');
        $q->addWhere('t.task_log_creator = "'.intVal($userID).'" AND t.task_log_date > "'.date('Y-m-d h:i:s',strtotime('-3 months')).'"');
        $q->addJoin('users', 'u', 'u.user_id = t.task_log_creator');	// Fetching Name for Owner, But maybe OwnerID does not exist anymore - so better to JOIN
        $q->addJoin('tasks', 'ts', 'ts.task_id = t.task_log_task');
        $q->addJoin('users', 'ut', 'ut.user_id = ts.task_owner');
        $q->addJoin('projects', 'p', 'p.project_id = ts.task_project');
        $q->addJoin('companies', 'c', 'c.company_id = p.project_company');
        $q->addOrder('t.task_log_id DESC');
        $q->addTable('task_log', 't');
        $sql = $q->prepare().' LIMIT 50';
        return db_loadList($sql);
   }

   public function addTaskLog($id,$desc,$name,$userID,$worked,$billable) {
        $date = new CDate();
        $q = new DBQuery();
        $q->addInsert("task_log_task", intVal($id));
        $q->addInsert("task_log_description", $desc);
        $q->addInsert("task_log_name", $name );
        $q->addInsert("task_log_creator",  intVal($userID) );
        $q->addInsert("task_log_hours", $worked);
        $q->addInsert("task_log_date", $date->format( FMT_DATETIME_MYSQL ));

        if($this->hasBillableHoursFeature()) {
            $q->addInsert("my_task_log_billable_hours", $billable);        
        }
        $q->addTable("task_log");
        $q->exec();
   }

   public function updateTaskProgress($id,$progress) {
        $q = new DBQuery();
        $q->addUpdate("task_percent_complete",$progress );
        $q->addWhere('task_id = "'.intVal($id).'"');
        $q->addTable("tasks");
        //$q->getQuery();
        $q->exec();
   }

   public function getUserId($user,$pass) {
        $q = new DBQuery();
        $q->addQuery('user_id');
        $q->addWhere('user_username = "'.mysql_real_escape_string($user).'" AND  user_password = "'.mysql_real_escape_string($pass).'"');
        $q->addTable('users');
        $sql = $q->prepare();
        $user = db_loadList($sql);
        return (count($user)==1)?$user[0]['user_id']:false;
   }

   public function getSysVal($key) {
        return dPgetSysVal($key);
   }

}
?>