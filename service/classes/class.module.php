<?php
/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

/**
 * Description of class module
 *
 * @author Tolleiv Nietsch
 */
abstract class module {

    abstract public function load($userID);
    abstract public function store($userID, DOMElement $dataElement );

    protected $dom;
    protected $domRoot;
    protected $dao;
    protected $response;




    function module() {
        $this->dom = new DOMDocument('1.0', 'utf-8');
        $this->domRoot = $this->dom->appendChild( $this->dom->createElement("root") );
    }

    function setDao($dao) {
        $this->dao = $dao;
    }

    function setResponseObj($res) {
        $this->response = $res;
    }

    // escaping/ encoding strings
    protected function out($str) {
        //return urlencode($str);
        return $str; 

    }

    // unescape // decode strings (e.g. xml attribute values)
    protected function in($str) {
        return urldecode($str);
    }

    protected function printOK($message = 'Execution successful') {
        $curElem = $this->domRoot->getElementsByTagName('response');
        if(!$curElem->length) {     //avoid multiple response-codes
            $resp = $this->dom->createElement('response');
            $resp->setAttribute('success','true');
            $msg = $this->dom->createElement('message');
            $msg->appendChild($this->dom->createTextNode($message));
            $resp->appendChild($msg);
            $this->domRoot->appendChild($resp);
        }
        $this->response->requestOk($this->dom->saveXML(),true);
    }

    protected function printError( $message ) {
        $this->response->requestFail($message);
    }
}
