<?php
/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

/**
 * Description of class response_stub
 *
 * @author Tolleiv Nietsch
 */
class response_http {
    const HTTP_BAD_REQUEST = '400 Bad Request';
    const HTTP_UNAUTHORIZED = '401 Unauthorized';
    const HTTP_OK = '200 OK';
    const HTTP_INTERNAL_SERVER_ERROR = '500 Internal Server Error';

    static private $instance = null;
    static public function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function requestOk($msg,$isXML=false) {
        header('HTTP/1.1 '.self::HTTP_OK);
        if($isXML) {
            header('Content-type: text/xml');
        }
        echo $msg;
        exit;
    }

    public function requestFail($msg='Request failed') {
        header('HTTP/1.1 '.self::HTTP_BAD_REQUEST);
        echo $msg;
        exit;
    }

    public function authFail($msg='Authentification failed') {
        header('HTTP/1.1 '.self::HTTP_UNAUTHORIZED);
        echo $msg;
        exit;
    }

    public function requestError($msg='Internal Server Error') {
        header('HTTP/1.1 '.self::HTTP_INTERNAL_SERVER_ERROR);
        echo $msg;
        exit;
    }

    public static function error_handler($num,$err,$file,$line) {

        // for some reason E_STRICT messages still pop up

        if(in_array(intval($num),array(E_STRICT,E_NOTICE,E_WARNING,E_DEPRECATED))) return;
        $r = new self;
        $r->requestError("$num ~ $err ($file [$line])");
        /*
        header('HTTP/1.1 500 '."$num ~ $err ($file [$line])");
        echo "$num ~ $err ($file [$line])<br/>";
        echo '<pre>'; var_dump(debug_backtrace());
         */
        exit;

    }
}

set_error_handler(array('response_http','error_handler'));

?>