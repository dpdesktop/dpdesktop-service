<?php
/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

/**
 * Description of class module_history
 *
 * @author Tolleiv Nietsch
 */
class module_history extends module {

    public function load($userID) {

        $history = $this->dao->getUserHistory($userID);

        foreach($history as $_) {
            //TODO find work-around to use strptime in windows, too
            
            $date = strptime($_['task_log_date'], FMT_DATETIME_MYSQL);
            $log = $this->dom->createElement('log');

            $log->setAttribute("id", $_['task_log_id']);
            $log->setAttribute("summary", $this->out($_['task_log_name']));
            $log->setAttribute("comment", $this->out($_['task_log_description']));
            $log->setAttribute("workedTime", $this->dao->convertFloatToDuration($_['task_log_hours']));
            $log->setAttribute("date", $_['task_log_date']);
            
            //$log->setAttribute("task_name", $this->out($_['task_name']));

            if($this->dao->hasBillableHoursFeature()) {
                $log->setAttribute("billableTime", $this->dao->convertFloatToDuration($_['my_task_log_billable_hours']));
            }
            $container = $this->dom->createElement("container");
            //$container->setAttribute("id");
            $priority = $this->dao->getSysVal("TaskPriority");

            $container->setAttribute("id", "ID-3-" . $_['task_id']);
            $container->setAttribute("owner", $this->out($_['user_username']) );
            $container->setAttribute("name", $this->out($_['task_name']) );
            $container->setAttribute("priority", $priority[$_['task_priority']] );
            $container->setAttribute("complete", $_['task_percent_complete']);
            $container->setAttribute("isTrackable", "true" );

            $log->appendChild($container);
            $this->domRoot->appendChild($log);
        }

        $this->printOk();

    }

    public function store($userID, DOMElement $dataElement) {

        $entries = $dataElement->getElementsByTagName("log");

  

        foreach($entries as $node) {

            $node = $entries->item(0);

            $id =  $node->getAttribute("containerID");

            list($x,$x,$id) = explode("-",$id);
            
            $task = $this->dao->getTaskById($id);
            if(count($task)) {                
                $workedHours = $this->dao->convertDurationToFloat($node->getAttribute("workedTime"));

                if($this->dao->hasBillableHoursFeature()) {
                    $billableHours = $this->dao->convertDurationToFloat($node->getAttribute("billableTime"));
                } else {
                    $billableHours = NULL;
                }
                $this->dao->addTaskLog(
                    $id,
                    $this->in($node->getAttribute('comment')),
                    $this->in( $node->getAttribute('summary')),
                    $userID,
                    $workedHours,
                    $billableHours
                );
                $this->dao->updateTaskProgress(
                    $id,
                    $node->getAttribute('complete')
                );
                $this->printOK();

            } else {

                $this->printError('Task ID is not valid. Maybe your selected task is too old. Updating your tracking tool could solve the problem.');

            }            
        }
    }
   


}


?>