<?php
/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

/**
 * Description of class module_factory
 *
 * @author Tolleiv Nietsch
 */
class module_factory {
    const moduls = 'container,history,config';
    public static function createModule($module,$dao,$responseClass) {
        $mod = strtolower($module);
        if(!in_array($mod,explode(',',self::moduls))) {
            return false;
        }
        include_once('classes/class.'.stripslashes($responseClass).'.php');
        include_once('classes/class.module_'.$mod.'.php');
        $modname = 'module_'.$mod;
        $m = new $modname();
        $m->setDao($dao);
        $m->setResponseObj(eval("return $responseClass::getInstance();"));
        return $m;
    }
}
?>
