<?php
/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

/**
 * Description of class dao
 *
 * @author Tolleiv Nietsch
 */
abstract class dao {

    protected $billableFeature = false;
    public function setBillableHoursFeature($enable=NULL) {
        $this->billableFeature = CONFIG_ENABLE_BILLABLE_TIME_FEATURE_IF_SUPPORTED && $enable;
    }


    /**
     * Be careful! You may convert the boolean value to a string, when
     * working with xml
     * @return <type> boolean
     */


    public function hasBillableHoursFeature() {
        return $this->billableFeature;
    }

    abstract public function getCompanies();
    abstract public function getProjectsAndCompanies();
    abstract public function getTaskById($id);
    abstract public function getUserTaskList($userID);
    abstract public function getUserHistory($userID);
    abstract public function addTaskLog($id,$desc,$name,$userID,$worked,$billable);

    public function convertDurationToFloat($duration) {
        list($hours,$minutes) = explode(":",$duration);
        return round(intval($hours) + (intval($minutes) / 60 ) +0.003, 3);
    }

    public function convertFloatToDuration($float) {
        $hours  =intval($float);
        $minutes=intval(($float-$hours)*60);
        return sprintf("%02d:%02d",$hours,$minutes);
    }

    abstract public function updateTaskProgress($id,$progress);
    abstract public function getUserId($user,$pass);
    abstract public function getSysVal($key);
}

if(defined('DPDesktopHost')) {
    if(file_exists('classes/class.dao_'.strtolower(DPDesktopHost).'.php')) {
        include_once('classes/class.dao_'.strtolower(DPDesktopHost).'.php');
        $daoclass = 'dao_'.strtolower(DPDesktopHost);
    }
}
?>