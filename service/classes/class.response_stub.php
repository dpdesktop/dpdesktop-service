<?php
/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

/**
 * Description of class response_stub
 *
 * @author Tolleiv Nietsch
 */
class response_stub {
    //static private $instance = null;
    static public function getInstance()
    {
        return new self;
    }

    public $invokations = 0;
    public $lastMessage = '';
    public $lastInvokation = '';
    public $containedError = false;
    public function requestOk($msg,$isXML=false) {
        $this->lastMessage = $msg;
        $this->invokations++;
        $this->lastInvokation = __FUNCTION__;
    }

    public function requestFail($msg='Request failed') {
        $this->lastMessage = $msg;
        $this->invokations++;
        $this->lastInvokation = __FUNCTION__;
        $this->containedError = true;
    }

    public function authFail($msg='Authentification failed') {
        $this->lastMessage = $msg;
        $this->invokations++;
        $this->lastInvokation = __FUNCTION__;
        $this->containedError = true;
    }
}
?>
