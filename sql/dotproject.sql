-- @Author Heiner Reinhardt
-- 
-- 
-- All additional table fields will have `my` as prefix 

-- --------------------------------------------------------

-- 
-- Patching table `task_log`
-- 

ALTER TABLE `task_log` ADD `my_task_log_billable_hours` FLOAT DEFAULT '0' NOT NULL ;
-- ADDING field `my_task_log_billable_hours`


